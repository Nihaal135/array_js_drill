/*function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
}*/
let nestedArray = [1, [2], [[3]], [[[4]]]];
let flattenFunc=require('../Problem/flatten');
const result=flattenFunc(nestedArray);
console.log(result);