/*function each(elements, cb) {
     Do NOT use forEach to complete this function.
     Iterates over a list of elements, yielding each in turn to the `cb` function.
     This only needs to work with arrays.
     You should also pass the index into `cb` as the second argument
     based off http://underscorejs.org/#each
}*/
let items=require('./itemList');
let each=require('../Problem/each');
function cb(element,index,elements){
     console.log(element);
}
each(items,cb);