/*function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
}*/
let items=require('./itemList');
let {filterFnc}=require('../Problem/filter');
function cb(item,index,arr){
    return item>2;
}
let result=filterFnc(items,cb);
console.log(result);