/*function each(elements, cb) {
     Do NOT use forEach to complete this function.
     Iterates over a list of elements, yielding each in turn to the `cb` function.
     This only needs to work with arrays.
     You should also pass the index into `cb` as the second argument
     based off http://underscorejs.org/#each
}*/
function each(items,cb){
     if(!Array.isArray(items) || !cb || !items){
          return [];
     }
     for(let  index=0; index < items.length; index++) {
          cb(items[index], index, items) ;
  
      }
}
module.exports=each;
