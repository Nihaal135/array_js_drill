/*function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order
    // and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
}*/
function mapFunc(items,cb){
    if(!Array.isArray(items) || !cb || !items){
        return undefined
    }
    let outputArray=[];
    for(let index=0;index<items.length;index++){
        let element=items[index];
        let cbResult=cb(element,index,items);
        outputArray.push(cbResult);
    }
    return outputArray;
}
module.exports=mapFunc;