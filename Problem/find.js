/*function find(elements, cb) {
     Do NOT use .includes, to complete this function.
     Look through each value in `elements` and pass each element to `cb`.
     If `cb` returns `true` then return that element.
     Return `undefined` if no elements pass the truth test.
}*/
//Input [1,2,3,4,5,5]
function find(items,cb){
    if(!Array.isArray(items) || !cb || !items){
        return undefined;
    }
    for(let index=0;index<items.length;index++){
        let item=items[index];
        if(cb(item,index,items)){
            return item;
        }
    }
    return undefined;
}
module.exports=find;