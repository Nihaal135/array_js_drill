/*function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
}*/
//[1, 2, 3, 4, 5, 5];Input.
function reduce(items,cb,startingValue){
    if(!Array.isArray(items) || !cb || !items){
        return[];
    }
    if(startingValue===undefined){
        startingValue=items[0];
    }
    else{
        startingValue=cb(startingValue,items[0]);
    }
    for(let index=1;index<items.length;index++){
        let element=items[index];
        startingValue=cb(startingValue,element);
    }
    return startingValue;
}
module.exports={reduce};