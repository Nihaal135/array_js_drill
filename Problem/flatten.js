/*function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
}*/
//[1, [2], [[3]], [[[4]]]]; Input.
function flattenFunc(nestedArray) {
    if (!Array.isArray(nestedArray) || !nestedArray ) {
      return [];
    }
    let stack=[...nestedArray];
    const output=[];
    while(stack.length){
        let element=stack.pop();
        if(Array.isArray(element)){
            stack.push(...element);
        }
        else{
            output.push(element);
        }
    }
    return output.reverse();
}
module.exports=flattenFunc;