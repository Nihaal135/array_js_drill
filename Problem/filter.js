/*function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
}*/
//Input [1,2,3,4,5,5]
function filterFnc(items,cb){
    if(!Array.isArray(items) || !cb || !items){
        return undefined;
    }
    let output=[];
    for(let index=0;index<items.length;index++){
        let item=items[index];
        if(cb(item,cb,items)){
            output.push(item);
        }
    }
    return output;
}
module.exports={filterFnc};